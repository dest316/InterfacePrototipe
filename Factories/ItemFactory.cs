﻿using InterfacePrototype.Components;
using SpaceVIL;
using SpaceVIL.Core;
using SpaceVIL.Decorations;
using System;
using System.Drawing;

namespace InterfacePrototype.Factories
{
    public static class ItemsFactory
    {
        public static HorizontalStack GetToolBarLayout()
        {
            HorizontalStack layout = new HorizontalStack();
            layout.SetHeightPolicy(SizePolicy.Fixed);
            layout.SetHeight(40);
            layout.SetBackground(35, 35, 35);
            layout.SetSpacing(3, 0);
            layout.SetPadding(10, 0, 0, 0);
            return layout;
        }

        public static VerticalStack GetSideBarLayout()
        {
            VerticalStack layout = new VerticalStack();
            layout.SetWidthPolicy(SizePolicy.Fixed);
            layout.SetWidth(40);
            layout.SetBackground(35, 35, 35);
            layout.SetMargin(0, 55, 0, 55);
            layout.SetContentAlignment(ItemAlignment.VCenter);
            return layout;
        }

        public static Label GetAreaForPermanentToolTip()
        {
            Label area = new Label();
            area.SetTextAlignment(ItemAlignment.VCenter, ItemAlignment.HCenter);
            area.SetMargin(55, 55, 55, 55);
            area.SetBackground(45, 45, 45);
            area.SetFontSize(25);
            area.SetText("ToolTip area.\nWhen the mouse cursor inside this item\nthe tooltip is always displayed.");
            area.SetBorder(new Border(Color.FromArgb(50, 50, 50), new CornerRadius(), 1));
            area.SetToolTip("It is the tooltip area.\nWhen the mouse cursor inside this item\nthe tooltip is always displayed.");
            area.EventMouseHover += (sender, args) =>
            {
                ToolTip.SetTimeOut(area.GetHandler(), 0);
            };
            area.EventMouseLeave += (sender, args) =>
            {
                ToolTip.SetTimeOut(area.GetHandler(), 300);
            };
            return area;
        }

        public static Prototype GetTool(Bitmap icon, String tooltip)
        {
            Tool tool = new Tool(icon, tooltip);
            return tool;
        }

        public static Prototype GetSideTool(Bitmap icon, SideToolTip tooltip)
        {
            Tool tool = new Tool(icon);
            tool.SetSizePolicy(SizePolicy.Expand, SizePolicy.Fixed);
            tool.SetHeight(40);
            tool.SetPadding(9, 9, 9, 9);
            tool.EventMouseHover += (sender, args) =>
            {
                tooltip.Show(sender, args);
            };
            tool.EventMouseLeave += (sender, args) =>
            {
                tooltip.Hide();
            };
            return tool;
        }

        public static IBaseItem GetHorizontalDivider()
        {
            SpaceVIL.Rectangle divider = new SpaceVIL.Rectangle();
            divider.SetHeight(1);
            divider.SetWidthPolicy(SizePolicy.Expand);
            divider.SetMargin(5, 10, 5, 10);
            divider.SetBackground(100, 100, 100);
            return divider;
        }

        public static IBaseItem GetVerticalDivider()
        {
            SpaceVIL.Rectangle divider = new SpaceVIL.Rectangle();
            divider.SetWidth(2);
            divider.SetHeightPolicy(SizePolicy.Expand);
            divider.SetMargin(0, 0, 0, 0);
            divider.SetBackground(100, 100, 100);
            return divider;
        }

        public static IBaseItem GetDecor()
        {
            Ellipse ellipse = new Ellipse(12);
            ellipse.SetSize(8, 8);
            ellipse.SetAlignment(ItemAlignment.VCenter, ItemAlignment.Left);
            ellipse.SetBackground(169, 89, 213);
            ellipse.SetMargin(5, 0, 0, 0);
            return ellipse;
        }
        public static IBaseItem GetGraphicWindow(CoreWindow window)
        {
            SpaceVIL.Rectangle graphicWindow = new SpaceVIL.Rectangle();
            graphicWindow.SetSize(400, 200);
            //graphicWindow.SetSize(window.GetWidth() - 200, window.GetHeight() - 200);
            graphicWindow.SetSizePolicy(SizePolicy.Expand, SizePolicy.Expand);
            graphicWindow.SetBackground(240, 240, 240);
            graphicWindow.SetMargin(95, 145, 50, 50);
            window.AddItem(graphicWindow);
            return graphicWindow;
        }
        public static IBaseItem GetHorizontalLinebar(CoreWindow window)
        {
            var widthLine = new SpaceVIL.Rectangle(); //Создание верхней линейки
            widthLine.SetSize(400, 15);
            widthLine.SetSizePolicy(SizePolicy.Expand, SizePolicy.Fixed);
            widthLine.SetBackground(210, 210, 210);
            widthLine.SetMargin(95, 165, 50, 50);
            window.AddItem(widthLine);
            for (int i = widthLine.GetX(); i < 1920; i += 100) //Нанесение разметки на верхнюю линейку
            {
                var tempLine = new SpaceVIL.Rectangle();
                tempLine.SetHeight(widthLine.GetHeight());
                tempLine.SetWidth(2);
                tempLine.SetBackground(32, 34, 37);
                window.AddItem(tempLine);
                tempLine.SetPosition(i, widthLine.GetX() - widthLine.GetHeight() + 53);                
            }
            return widthLine;
        }
        public static IBaseItem GetVerticalLinebar(CoreWindow window)
        {
            var heightLine = new SpaceVIL.Rectangle(); //Создание верхней линейки
            heightLine.SetSize(15, 400);
            heightLine.SetSizePolicy(SizePolicy.Fixed, SizePolicy.Expand);
            heightLine.SetBackground(210, 210, 210);
            heightLine.SetMargin(100, 145, 0, 50);
            window.AddItem(heightLine);
            
            for (int i = heightLine.GetY(); i < 1080; i += 100) //Нанесение разметки на боковую линейку
            {
                var tempLine = new SpaceVIL.Rectangle();
                tempLine.SetHeight(2);
                tempLine.SetWidth(heightLine.GetWidth());
                tempLine.SetBackground(32, 34, 37);
                window.AddItem(tempLine);
                tempLine.SetPosition(heightLine.GetY() - heightLine.GetWidth() - 47, i);
            }
            return heightLine;
        }
        public static ComboBox GetComboBox(params string[] items)
        {
            ComboBox comboBox = new ComboBox();
            comboBox.SetSizePolicy(SizePolicy.Fixed, SizePolicy.Fixed);
            comboBox.SetSize(200, 20);
            foreach (var item in items)
            {
                comboBox.AddItem(new MenuItem(item));
            }
            return comboBox;
        }
        public static ButtonCore GetButton(string label)
        {
            ButtonCore button = new ButtonCore();
            button.SetSizePolicy(SizePolicy.Fixed, SizePolicy.Fixed);
            button.SetSize(150, 20);
            button.SetText(label);
            return button;
        }
        public static TextEdit GetTextField(string tip)
        {
            TextEdit textEdit = new TextEdit(tip);
            textEdit.SetSizePolicy(SizePolicy.Fixed, SizePolicy.Fixed);
            textEdit.SetSize(160, 40);
            
            return textEdit;
        }
    }
}
