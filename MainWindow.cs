﻿using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using InterfacePrototype.Components;
using InterfacePrototype.Factories;
using SpaceVIL;
using SpaceVIL.Common;
using SpaceVIL.Core;
using SpaceVIL.Decorations;

namespace InterfacePrototype
{
    public class MainWindow : ActiveWindow
    {
        public override void InitWindow()
        {
            SetParameters(nameof(MainWindow), nameof(MainWindow), 800, 600);
            SetMinSize(800, 600);
            SetBackground(32, 34, 37);
            SetMaxSize(800, 600);
            

            //код для моделирования
            /*
            VerticalStack sideBar = ItemsFactory.GetSideBarLayout();
            HorizontalStack horizontalBar = ItemsFactory.GetToolBarLayout();
            // add items to window
            AddItems(
                sideBar,
                horizontalBar
            //ItemsFactory.GetAreaForPermanentToolTip() //TODO: here we should set up graphical area
            );
            
            string pathToImages = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\..", @"Images\"); //TODO: move to config
            sideBar.AddItems(
                ItemsFactory.GetSideTool(new Bitmap(Path.Combine(pathToImages, "wall512-512.png")),
                    new SideToolTip(this, "Wall:\nCreates a new wall.")),
                ItemsFactory.GetSideTool(new Bitmap(Path.Combine(pathToImages, "dynamic920-400.png")),
                    new SideToolTip(this, "Source:\nCreates a new source.")),
                ItemsFactory.GetSideTool(new Bitmap(Path.Combine(pathToImages, "receiver920-512.png")),
                    new SideToolTip(this, "Receiver:\nCreates a new receiver area")),
                ItemsFactory.GetSideTool(new Bitmap(Path.Combine(pathToImages, "floor512-512.png")),
                    new SideToolTip(this, "Floor:\nCreates a floor area.")),
                ItemsFactory.GetSideTool(new Bitmap(Path.Combine(pathToImages, "ceiling512-512.png")),
                    new SideToolTip(this, "Ceiling:\nCreates a ceiling area.")),
                ItemsFactory.GetSideTool(new Bitmap(Path.Combine(pathToImages, "trash512-512.png")),
                    new SideToolTip(this, "Clear:\nRemove all object from the canvas")),
                ItemsFactory.GetSideTool(new Bitmap(Path.Combine(pathToImages, "object512-512.png")), //для лекционных аудиторий
                    new SideToolTip(this, "Object:\nCreates an new moveable object in building"))
            );

            horizontalBar.AddItems(
                ItemsFactory.GetSideTool(new Bitmap(Path.Combine(pathToImages, "profile512-512.png")),
                    new SideToolTip(this, "Profile:\nGo to setup your account.")),
                ItemsFactory.GetSideTool(new Bitmap(Path.Combine(pathToImages, "Info512-512.png")),
                    new SideToolTip(this, "Info:\nRead documentation for this program.")),
                ItemsFactory.GetSideTool(new Bitmap(Path.Combine(pathToImages, "import512-512.png")),
                    new SideToolTip(this, "Import:\nImport a new preferences for this model")),
                ItemsFactory.GetSideTool(new Bitmap(Path.Combine(pathToImages, "settings512-512.png")),
                    new SideToolTip(this, "Settings:\nChange program parameters."))
            );
            var gw = ItemsFactory.GetGraphicWindow(this) as SpaceVIL.Rectangle;
            gw.SetPosition(100, 150);
            var widthLine = ItemsFactory.GetHorizontalLinebar(this) as SpaceVIL.Rectangle;
            widthLine.SetPosition(100, 135);
            var heightLine = ItemsFactory.GetVerticalLinebar(this) as SpaceVIL.Rectangle;
            heightLine.SetPosition(85, 150);
            */

            ComboBox tasks = ItemsFactory.GetComboBox("Creating model", "Set parameters");
            AddItem(tasks);
            tasks.SetPosition(50, 70);

            ComboBox buildingTypes = ItemsFactory.GetComboBox("Living quaters", "Lecture hall");
            AddItem(buildingTypes);
            buildingTypes.SetPosition(300, 70);

            ButtonCore enterChangesButton = ItemsFactory.GetButton("Save");
            AddItem(enterChangesButton);
            enterChangesButton.SetPosition(520, 70);

            //код для задания параметров

            TextEdit volume = ItemsFactory.GetTextField("Enter volume");
            AddItem(volume);
            volume.SetPosition(100, 150);

            /*TextEdit requiredReverbirationTime = ItemsFactory.GetTextField("Enter required reverbiration time");
            AddItem(requiredReverbirationTime);
            requiredReverbirationTime.SetPosition(100, 210); //Только для лекционок

            TextEdit maximalStudentsCount = ItemsFactory.GetTextField("Enter maximal count of students");
            AddItem(maximalStudentsCount);
            maximalStudentsCount.SetPosition(100, 270); //Только для лекционок*/

            ButtonCore confirmButton = ItemsFactory.GetButton("Confirm");
            AddItem(confirmButton);
            confirmButton.SetPosition(350, 210);
        }
        
    }
}
